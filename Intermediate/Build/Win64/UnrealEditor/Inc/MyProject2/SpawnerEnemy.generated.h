// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT2_SpawnerEnemy_generated_h
#error "SpawnerEnemy.generated.h already included, missing '#pragma once' in SpawnerEnemy.h"
#endif
#define MYPROJECT2_SpawnerEnemy_generated_h

#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_SPARSE_DATA
#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_RPC_WRAPPERS
#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnerEnemy(); \
	friend struct Z_Construct_UClass_ASpawnerEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnerEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerEnemy)


#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpawnerEnemy(); \
	friend struct Z_Construct_UClass_ASpawnerEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnerEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerEnemy)


#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnerEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnerEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerEnemy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerEnemy(ASpawnerEnemy&&); \
	NO_API ASpawnerEnemy(const ASpawnerEnemy&); \
public:


#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerEnemy(ASpawnerEnemy&&); \
	NO_API ASpawnerEnemy(const ASpawnerEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerEnemy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnerEnemy)


#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_10_PROLOG
#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_SPARSE_DATA \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_RPC_WRAPPERS \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_INCLASS \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_SPARSE_DATA \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_INCLASS_NO_PURE_DECLS \
	FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT2_API UClass* StaticClass<class ASpawnerEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_MyProject2_Source_MyProject2_Public_SpawnerEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
